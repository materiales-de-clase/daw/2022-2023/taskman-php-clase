<?php
// Crea una etiqueta y retorna los datos de la misma

    // Pasa la entrada a json
    $json = file_get_contents('php://input');

    // Aquí tenemos un array con los parámetros de entrada
    $objeto = json_decode($json, TRUE);

    // Obtiene los valores que vienen en el array asociativo
    
    $titulo = $objeto["titulo"];
    $idInformador = $objeto["informadorId"];
    $idAsignado = $objeto["asignado"];
    $tipo = $objeto["tipo"];
    $estado = $objeto["estado"];
    $descripcion = $objeto["descripcion"];
    $fechaAlta = $objeto["fechaalta"];
    $fechaVencimiento = $objeto["fechavencimiento"];
    $horaVencimiento = $objeto["horavencimiento"];

    $tarea = new Tarea(
        -1,
        $titulo,
        $idInformador,
        $idAsignado,
        $tipo,
        $estado,
        $descripcion,
        $fechaAlta,
        $fechaVencimiento,
        $horaVencimiento
    );

    // Obtiene la lógica de negocio
    $lnTareas = LnTareas::singletonTareas();

    // Inserta la tarea
    $tarea = $lnTareas->addTarea($tarea);

    // Prepara la respuesta en caso de éxito
    $respuesta['ok'] = "1";
    $respuesta['mensaje'] = "";
    $respuesta['datos'] = [
        "id" => $tarea->getIdTarea(),
        "titulo" => $tarea->getTitulo(),
        "informadorId" => $tarea->getIdInformador(),
        "asignado" => $tarea->getIdAsignado(),
        "tipo" => $tarea->getTipo(),
        "estado" => $tarea->getEstado(),
        "descripcion" => $tarea->getDescripcion(),
        "fechaalta" => $tarea->getFechaAlta(),
        "fechavencimiento" => $tarea->getFechaVencimiento(),
        "horavencimiento" => $tarea->getHoraVencimiento()
    ];
?>
