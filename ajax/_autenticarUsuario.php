<?php
// Retorna los usuarios pasado el nombre a buscar

    // Pasa la entrada a json
    $json = file_get_contents('php://input');

    // Aquí tenemos un array con los parámetros de entrada
    $objeto = json_decode($json, TRUE);

    // Obtiene los valores que vienen en el array asociativo
    $usuario    = $objeto["usuario"];
    $pass       = $objeto["pass"];

    // Obtiene la lógica de negocio de Tareas
    $lnSesion = LnSesion::singletonLnSesion();

    // Obtiene la lista de tareas
    $usuario = $lnSesion->iniciar($usuario, $pass);

    // Prepara la respuesta en caso de éxito
    if($usuario != null) {
        $respuesta['ok'] = 1;
        $respuesta['mensaje'] = "";
        $respuesta['datos'] = [        
        
            "id_usuario" => $usuario->getIdUsuario(),
            "usuario" => $usuario->getUsuario(),
            "nombre_completo" => $usuario->getNombreCompleto(),
            "rol" => $usuario->getRol()
        ];
    }
?>
