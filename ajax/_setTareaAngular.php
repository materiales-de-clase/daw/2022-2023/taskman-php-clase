<?php
// Crea una etiqueta y retorna los datos de la misma

    // Pasa la entrada a json
    $json = file_get_contents('php://input');

    // Aquí tenemos un array con los parámetros de entrada
    $objeto = json_decode($json, TRUE);

    // Obtiene los valores que vienen en el array asociativo
    
    $idTarea = $objeto["id_tarea"];
    $titulo = $objeto["titulo"];
    $idInformador = $objeto["id_informador"];
    $idAsignado = $objeto["id_asignado"];
    $tipo = $objeto["id_tipo_tarea"];
    $estado = $objeto["id_estado"];
    $descripcion = $objeto["descripcion"];
    $fechaAlta = $objeto["fecha_alta"];
    $fechaVencimiento = $objeto["fecha_vencimiento"];
    $horaVencimiento = $objeto["hora_vencimiento"];

    $tarea = new Tarea(
        $idTarea,
        $titulo,
        $idInformador,
        $idAsignado,
        $tipo,
        $estado,
        $descripcion,
        $fechaAlta,
        $fechaVencimiento,
        $horaVencimiento
    );

    // Obtiene la lógica de negocio
    $lnTareas = LnTareas::singletonTareas();

    // Inserta la tarea
    $tarea = $lnTareas->setTarea($tarea);

    // Prepara la respuesta en caso de éxito
    $respuesta['ok'] = 1;
    $respuesta['mensaje'] = "";
    $respuesta['datos'] = [
        "id_tarea" => $tarea->getIdTarea(),
        "titulo" => $tarea->getTitulo(),
        "id_informador" => $tarea->getIdInformador(),
        "id_asignado" => $tarea->getIdAsignado(),
        "id_tipo_tarea" => $tarea->getTipo(),
        "id_estado" => $tarea->getEstado(),
        "descripcion" => $tarea->getDescripcion(),
        "fecha_alta" => $tarea->getFechaAlta(),
        "fecha_vencimiento" => $tarea->getFechaVencimiento(),
        "hora_vencimiento" => $tarea->getHoraVencimiento()
    ];
?>
