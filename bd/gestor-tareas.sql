----------------------------------------------------------------
-- Crea la base de datos
----------------------------------------------------------------
create database taskman;

----------------------------------------------------------------
-- Crea la tabla de usuarios
--
-- En esta tabla se incluye además el rol del usuario. El rol
-- actualmente es una constante.
----------------------------------------------------------------
create table usuarios (

    id_usuario      int not null auto_increment,
    usuario         varchar(16) not null,
    password        varchar(32) not null,
    nombre_completo varchar(200),

    rol             varchar(100) default 'usuario',

    primary key (id_usuario),
    constraint usuarios_uq_usuario unique (usuario)
);

insert into usuarios (usuario, password, nombre_completo, rol) values ('admin',   '1234', 'Administrador', 'admin');
insert into usuarios (usuario, password, nombre_completo, rol) values ('usuario', '1234', 'Usuario', 'usuario');


----------------------------------------------------------------
-- Crea la tabla de tipos de tareas
--
-- En esta tabla se encuentran los tipos de tareas que se van
-- a implementar en la aplicación 
----------------------------------------------------------------
create table tipos_tarea (
    id_tipo_tarea        int not null auto_increment,
    nombre               varchar(100) not null,

    primary key (id_tipo_tarea)
);

insert into tipos_tarea (id_tipo_tarea, nombre) values (1, 'Tarea');
insert into tipos_tarea (id_tipo_tarea, nombre) values (2, 'Recordatorio');

----------------------------------------------------------------
-- Crea la tabla de estados
--
-- Para cada tipo de tarea podemos tener una lista diferente de
-- estados. Estos estados serán los estados en que se puede
-- poner una tarea.
----------------------------------------------------------------
create table estados_tipo_tarea (
    id_tipo_tarea        int not null,
    id_estado            int not null auto_increment,
    
    nombre               varchar(100) not null,

    primary key (id_estado),
    foreign key (id_tipo_tarea) references tipos_tarea(id_tipo_tarea) on delete cascade
);

insert into estados_tipo_tarea (id_tipo_tarea, id_estado, nombre) values (1, 1, "Abierta");
insert into estados_tipo_tarea (id_tipo_tarea, id_estado, nombre) values (1, 2, "En progreso");
insert into estados_tipo_tarea (id_tipo_tarea, id_estado, nombre) values (1, 3, "Cerrada");

insert into estados_tipo_tarea (id_tipo_tarea, id_estado, nombre) values (2, 4, "Abierto");
insert into estados_tipo_tarea (id_tipo_tarea, id_estado, nombre) values (2, 5, "Cerrado");

----------------------------------------------------------------
-- Etiquetas
--
-- Esta es la tabla de etiquetas. Las etiquetas permiten
-- clasificar las tareas
----------------------------------------------------------------
create table etiquetas (
    id_etiqueta        int not null auto_increment,
    nombre             varchar(50) not null,

    primary key(id_etiqueta),
    constraint unique(nombre)
);

insert into etiquetas (id_etiqueta, nombre) values (1, "Urgente");
insert into etiquetas (id_etiqueta, nombre) values (2, "Poco importante");

----------------------------------------------------------------
-- Crea la tabla de tareas
--
-- Al ser una aplicación de gestión de tareas, la tabla de 
-- tareas va a ser el núcleo de las tablas de la aplicación.
----------------------------------------------------------------
create table tareas (

    id_tarea        int not null auto_increment,
    titulo          varchar(100) not null,

    id_informador   int not null, -- ID del usuario que registra la 
                                  -- incidencia
    id_asignado     int not null, -- Persona asignada a la incidencia
                                  -- por defecto será el informador

    tipo            int not null, -- Es el tipo de tareas. 
                                  -- los tipos de incidencia se descargan
                                  -- desde la tabla de tipos de tareas.
    
    estado          int not null, -- Estado en que se encuentra la tarea
                                  
    descripcion     varchar(4000),-- descripción de la tarea

    fecha_alta      timestamp default now(), 
        
    fecha_vencimiento date,     -- Fecha de vencimiento de la tarea
    hora_vencimiento time,      -- Hora de vencimiento de la tarea

    primary key (id_tarea)
);

INSERT INTO tareas
(titulo, id_informador, id_asignado, tipo, estado, descripcion, fecha_alta, fecha_vencimiento, hora_vencimiento)
VALUES('Ejemplo de tarea', 1, 2, 1, 2, 'current_timestamp()', current_timestamp(), NULL, NULL);


----------------------------------------------------------------
-- Etiquetas
--
-- Cada tarea puede tener asociados un determinado númnero
-- de etiquetas.
----------------------------------------------------------------
create table etiquetas_tarea (
    id_tarea        int not null,
    id_etiqueta     int not null, 

    primary key(id_tarea, id_etiqueta),

    constraint fk_etiquetas_tarea_id_etiqueta foreign key (id_etiqueta) references etiquetas(id_etiqueta) on delete cascade,
    constraint fk_etiquetas_tarea_id_tarea foreign key (id_tarea) references tareas(id_tarea) on delete cascade
);


----------------------------------------------------------------
-- Comentarios
--
-- Comentarios asociados a la tarea. Cada comentario
-- está asociado a una tarea y un usuario.
----------------------------------------------------------------
create table comentarios (
    id_tarea        int not null,
    id_comentario   int not null auto_increment,

    fecha_alta      timestamp default now(), 
    texto           varchar(1000) not null,

    primary key (id_comentario),
    constraint fk_comentarios_id_tarea foreign key (id_tarea) references tareas(id_tarea) on delete cascade
);

