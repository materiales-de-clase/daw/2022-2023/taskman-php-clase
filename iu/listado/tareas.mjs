import { TASKMAN_DOCUMENT_BASE } from '../../js/environment.mjs';
import { mostrarAlert, mostrarModalEliminar } from '../../js/dialogos.mjs';
import { AxTabla } from '../../js/tabla.mjs';
import { axEliminarTarea, axGetRegistro } from '../../js/axtareas.mjs';

import { frmInicializar, frmAsignarFormulario } from '../../js/frm-bootstrap.mjs';
import { frmInicializarFormulario, frmValidarFormulario } from '../../js/frm-validaciones-bootstrap.mjs';
import { axInicializarDatalist } from '../../js/ajax_datalist.mjs';
import { axInicializarSelect , axInicializarSelectEsclavo, axAsignarSelectEsclavo } from '../../js/ajax_select.mjs'; 


//----------------------------------------------------------------
// CONSTANTES
//----------------------------------------------------------------

const PLANTILLA = {
    '<>': 'tr','html': [
        {'<>': 'td','html': '${id_tarea}'},
        {'<>': 'td','html': '${titulo}'},
        {'<>': 'td','html': '${tipo}'},
        {'<>': 'td','html': '${estado}'},
        {'<>': 'td','html': '${fecha_vencimiento}'},
        {'<>': 'td','html': '${hora_vencimiento}'},
        {'<>':'td','html':'<button class="botonActualizar btn btn-info bi bi-pencil-fill" value="${id_tarea}"></button>'},
        {'<>':'td','html':'<button class="botonEliminar btn btn-danger bi bi-trash-fill" value="${id_tarea}"></button>'}
    ]}; 

const URL_GET_TAREAS_POR_TITULO = `${TASKMAN_DOCUMENT_BASE}/ajax.php?s=_getTareasPorTitulo&__debug`;
const URL_GET_TAREA             = `${TASKMAN_DOCUMENT_BASE}/ajax.php?s=_getTarea&__debug`;
const URL_SET_TAREA             = `${TASKMAN_DOCUMENT_BASE}/ajax.php?s=_setTarea&__debug`;
const URL_ADD_TAREA             = `${TASKMAN_DOCUMENT_BASE}/ajax.php?s=_addTarea&__debug`;
const URL_GET_USUARIOS_NOMBRE   = `${TASKMAN_DOCUMENT_BASE}/ajax.php?s=_getSelectUsuariosPorNombre&__debug`;
const URL_GET_TIPOS_TAREA       = `${TASKMAN_DOCUMENT_BASE}/ajax.php?s=_getSelectTiposTarea&__debug`;
const URL_GET_ESTADOS_TAREA     = `${TASKMAN_DOCUMENT_BASE}/ajax.php?s=_getSelectEstadosTareaPorTipo&__debug`;

// Crea el objeto que gestiona la tabla
const tabla = new AxTabla(
    PLANTILLA, 
    URL_GET_TAREAS_POR_TITULO, 
    'cuerpoTabla'
);


//----------------------------------------------------------------
// Inicializaciones varias
//----------------------------------------------------------------

// Llama a inicializar los campos del formulario
inicializarFormulario();

// Inicializar formulario busqueda
inicializarFormularioBusqueda();

// Renderiza la tabla
tabla.renderizar();


//----------------------------------------------------------------
// Inicializa el botón de alta
//----------------------------------------------------------------

// Asigna el gestor de evento para hacer un alta
$('#botonAlta').click((evento) => {

    // Inicializa los campos del formulario al valor por defecto
    resetearCamposFormulario();

    // Pone el formulario en modo alta
    $('#frmtarea').attr('action', URL_ADD_TAREA);

    // Muestra el formulario
    $('#edicionTarea').modal('show');
});



//----------------------------------------------------------------
// BOTONES EN LA TABLA 
// Aquí implemento los gestores de eventos de los botones
// que tiene cada uno de los registros en la tabla.
//----------------------------------------------------------------

// Evento para el botón eliminar
$('#cuerpoTabla').on('click', '.botonEliminar',  (event) => {
    
    // Obtengo el boton
    const boton = event.target;

    // Obtengo el identificador
    const id = boton.value;

    // Obtengo el título de la tarea
    const nombre = $(boton).parents('tr').children('td:nth-child(2)').text();

    mostrarModalEliminar(`¿Está seguro de que quiere eliminar la tarea ... ${nombre}`, () => {

        axEliminarTarea(
            id, 
            // onOK
            () => {
                $(boton).parents('tr').remove();
            },

            // onError
            (mensaje) => {
                mostrarAlert('Error: '+mensaje);
            });
    });

});


// Evento para el botón actualizar
$('#cuerpoTabla').on('click', '.botonActualizar',  (event) => {
    
    // Obtengo el boton
    const boton = event.target;

    // Obtengo el identificador
    const id = boton.value;

    axGetRegistro(
        URL_GET_TAREA, 
        id,         
        (tarea) => { // onOK

            // Carga la tarea en el formulario
            db2formulario(tarea);

            // Pone el form en modo edición
            $('#frmtarea').attr('action', URL_SET_TAREA);

            // Muestra el cuadro de dialogo
            $('#edicionTarea').modal('show');
        },         
        (mensaje) => { // onError
            mostrarAlert(mensaje);
        }
    );
});

// Inicializa los campos del formulario
axInicializarSelect($('#tipo')[0], 'ajax.php?s=_getSelectTiposTarea&__debug');


// Muestra la tabla al completo
tabla.renderizar();

// Inicializa el formulario
frmInicializar(    
    $('#edicionTarea')[0], 

    // onOK
    () => {
        enviarConsultaFiltro();
    },
    //onError
    (mensaje) => {
        mostrarAlert(mensaje);
    }
);


//----------------------------------------------------------------
// FILTRO / BUSCADOR DE TAREAS
//
// Inicialización y funciones implementadas relacionadas con el 
// filtro o buscador
//----------------------------------------------------------------

function inicializarFormularioBusqueda() {
    // Inicializa el formulario
    $('#frmBusqueda').on('submit', enviarConsultaEvento);
}

/**
 * Envía la consulta al servicio y refresca la tabla  
 * 
 * @param {*} event 
 */
 function enviarConsultaEvento(event) {

    // Previene que se haga el submit
    event.preventDefault();

    enviarConsultaFiltro();
}

/**
 * Envía la consulta que hay actualmente en el filtro 
 * 
 * @param {*} event 
 */
function enviarConsultaFiltro() {

    // Obtengo el filtro
    const filtro = $('#frmBusqueda #filtroTabla')[0].value;

    tabla.renderizar(filtro);    
}




//----------------------------------------------------------------
// Gestión del formulario 
//----------------------------------------------------------------
    
// Inicializa el formulario
function inicializarFormulario() {

    // Inicializa el formulario
    frmInicializarFormulario($('#frmtarea')[0]);

    // Inicializa el datalist del informador
    inicializarInformadorDatalist();

    // Inicializa el Select del informador
    inicializarAsignadoSelect();

    // Inicializa el Select del Tipo
    inicializarTipoSelect();

    // Inicializa el Select del estado
    inicializarEstadoSelect();
}


$('#botonGuardar').on('click', (event) => {

    frmValidarFormulario(
        $('#frmtarea')[0],
        
        // onOk
        () => {
        
            // Enviar los datos
            enviarFormulario();
        },
        
        // onError
        () => {
            // Los errores de validación ya se muestran en el formulario
        }
    );
});


/**
 * Envía el formulario al servidor
 */
 function enviarFormulario() {   

    // Obtiene el formulario
    const formularioRef = $('#frmtarea')[0];

    // Creo un objeto para generar los parámetros
    const tarea = formulario2db(formularioRef);

    fetch(
        formularioRef.action,
        {
            method: 'POST',
            body: JSON.stringify(tarea),
            headers: {
                'Content-Type': 'application/json'
            }
        }

    )
    .then(respuesta => respuesta.json())
    .then(respuesta => {
        mostrarResultado(formularioRef, respuesta);
    });
}


function mostrarResultado(formularioRef, respuesta) {

    if(respuesta.ok == 1) {

        // Cierra la ventana
        $('#edicionTarea').modal('hide');        

        // Refresca la tabla
        tabla.renderizar();
        
    } else {
        // Muestra el mensaje de error
        mostrarAlert(respuesta.mensaje);
    }

}


//----------------------------------------------------------------
// Funciones del formulario
//----------------------------------------------------------------

// Resetea los campos del formulario
function resetearCamposFormulario() {
    $('#frmtarea')[0].reset();
}

// Retorna un objeto con los datos en el formulario 
function formulario2db(formulario) {
    
    const tarea = {};
 
    // Identificador
    tarea.tareaId = $('#tareaId').val();

    // Inicializa el formulario con los datos de la tarea
    tarea.titulo = $('#titulo').val();
            
    // Es un datalist
    tarea.informador = $('#informador').val();
    tarea.informadorId = $('#informadorId').val();

    // Un select normal
    tarea.asignado = $('#asignado').val();
    tarea.tipo = $('#tipo').val();
    tarea.estado = $('#estado').val();

    // Son fechas
    tarea.fechaalta = $('#fechaalta').val();
    tarea.fechavencimiento = $('#fechavencimiento').val();
    tarea.horavencimiento = $('#horavencimiento').val();

    // Otros campos
    tarea.descripcion = $('#descripcion').val();

    return tarea;
}

// Entra una tarea que viene de la base de datos y la carga en el formulario
function db2formulario(tarea) {
    // Inserta los valores en datos dentro de los campos del formulario
    //frmAsignarFormulario($('#frmtarea')[0], datos);
    // Asigna el ID de tarea
    $('#tareaId').val(tarea.id_tarea);

    // Inicializa el formulario con los datos de la tarea
    $('#titulo').val(tarea.titulo);
    
    // Es un datalist
    $('#informador').val(tarea.informador);
    $('#informadorId').val(tarea.id_informador);

    // Un select normal
    $('#asignado').val(tarea.id_asignado);
    $('#tipo').val(tarea.id_tipo_tarea);

    // Pone en estado el valor y carga el select
    axAsignarSelectEsclavo($('#estado')[0], tarea.id_estado);

    // Son fechas
    $('#fechaalta').val(tarea.fecha_alta);
    $('#fechavencimiento').val(tarea.fecha_vencimiento);
    $('#horavencimiento').val(tarea.hora_vencimiento);

    // Otros campos
    $('#descripcion').val(tarea.descripcion);

    // Pone el form en modo edición
    $('#frmtarea').attr('action', 'ajax.php?s=_setTarea');
}


//-------------------------------------------------------------------------------
// Inicializar componentes
//-------------------------------------------------------------------------------

// Inicializa el Datalist del informador
function inicializarInformadorDatalist() {

    // Obtiene referencia a los campos que se necesitan
    let input = document.getElementById('informador');
    let hinput = document.getElementById('informadorId');
    let dl = document.getElementById('informadorDatalistOptions');        

    // Inicializa el campo Datalist
    axInicializarDatalist(input, hinput, dl, URL_GET_USUARIOS_NOMBRE);
}


// Inicializa el Datalist del informador
function inicializarAsignadoSelect() {

    // Obtiene referencia a los campos que se necesitan
    let select = document.getElementById('asignado');

    // Inicializa el campo Datalist
    axInicializarSelect(select, URL_GET_USUARIOS_NOMBRE);
}

// Inicializa el Select del informador
function inicializarTipoSelect() {

    // Obtiene referencia a los campos que se necesitan
    let tipo = document.getElementById('tipo');

    // Inicializa el campo Datalist
    axInicializarSelect(tipo, URL_GET_TIPOS_TAREA);
}

// Inicializa el Select del estado. Este select depende del tipo
// cuando cambia el tipo, se debe descargar
function inicializarEstadoSelect() {

    // Obtiene referencia a los campos que se necesitan
    let tipo = document.getElementById('tipo');
    let estado = document.getElementById('estado');

    // Inicializa el campo Datalist
    axInicializarSelectEsclavo(tipo, estado, URL_GET_ESTADOS_TAREA);
}


