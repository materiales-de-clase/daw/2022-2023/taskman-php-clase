<script type="module" src="iu/listado/tareas.mjs"></script>

<form id="frmBusqueda">        
    <div class="row mb-1">    
        <div class="col mb-1">
            <input id="filtroTabla" name="filtroTabla" type="text" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary col-md-2 mb-1">Enviar</button>
        <button id="botonAlta" name="botonAlta" value="Guarda" type="button" 
                class="btn btn-warning col-md-1 text-white mb-1">
            <i class="bi bi-plus-lg text-white"></i>
        </button>                            

    </div>
</form>

<!-- Aquí está la tabla donde se van a insetar los resultados -->
<table class="table table-bordered table-striped text-center table-light table-hover align-items-center">

    <!-- Encabezado de la tabla -->
    <thead class="table text-white bg-primary">
        <tr>
            <th scope="col">id</th>
            <th scope="col">Título</th>
            <th scope="col">Tipo</th>
            <th scope="col">Estado</th>
            <th scope="col">Fecha Vencimiento</th>
            <th scope="col">Hora Vencimiento</th>
            <th scope="col"></th> <!-- Mostrar detalles / Editar -->
            <th scope="col"></th> <!-- Eliminar tarea -->
        </tr>
    </thead>
    
    <!-- 
        Cuerpo de la tabla. Aquí es donde se van a insertar los registros 
        devueltos por la consulta SQL
     -->
    <tbody id='cuerpoTabla'>
    </tbody>
</table>

<!--  
    Cuadro de diálogo donde va a aparecer la modificación de tareas
    
    Es de tipo "Vertically centered scrollable modal"
 -->
 <div class="modal fade" id="edicionTarea" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar tarea</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <?php include 'iu/frm/frmtarea.php' ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        <button id="botonGuardar" type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>
