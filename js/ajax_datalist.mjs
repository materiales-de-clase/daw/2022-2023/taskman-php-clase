// -----------------------------------------------------------------------
// Implementa la gestión de un DataList utilizando AJAX
// Requiere para funcionar lo siguiente:
//  - El input donde el usuario va a escribir los datos
//  - El input oculto donde se va a almacenar el ID que se va a enviar al servidor
//  - El datalist al que volcar los datos procedentes del servidor
//  - La URL de descarga a la que se va a enviar contenido del input para hacer
//      la búsqueda
// Eventos que se van a gestionar
//  - Al escribir, se van a ir cargando datos una vez hay al menos 3 letras
//  - Cuando se selecciona un elemento se asigna al usuario
// -----------------------------------------------------------------------
export { axInicializarDatalist };

const PLANTILLA_DATALIST = {'<>': 'option','data-value':'${id}','html': '${texto}' };

/**
 * Inicializa el DataList y lo deja preparado para hacer las búsquedas
 * 
 * @param {
 * } input 
 * @param {*} datalist  
 * @param {*} url
 */
function axInicializarDatalist(input, hinput, datalist, url) {

    // Asigna un evento para controlar la entrada del usuario
    input.addEventListener('input', onUserInput);
    input.addEventListener('change', onChange);

    // Guarda todos los datos que se van a necesitar posteriormente
    input._hinput = hinput;
    input._datalist = datalist;
    input._url = url;
}


/**
 * Función que se invoca en caso de que el usuario pulse una tecla en el input
 * 
 * @param {*} e 
 */
function onUserInput(e) {

    // Obtiene referencia de los objetos que necesita
    let input = e.target;    
    let hinput = input._hinput;
    let datalist = input._datalist;
    let url = input._url;
    let value = input.value;

    // Si se cumplen estas condiciones, se está insertando el texto
    // del Datalist en el input. En este caso hay que tomar el ID
    // y pasarlo al hidden
    if(!e.inputType || e.inputType == 'insertReplacementText') {

        copiarId(value, datalist, hinput);

    } else {

        // Pone el ID a 0. Ya que hemos escrito en el campo
        hinput.value = '-1';

        // Obtiene el valor
        actualizarDataList(value, datalist, url);
    }
}

/**
 * Función que se invoca cuando se sale del campo y ha cambiado el valor
 * 
 * @param {*} e 
 */
function onChange(e) {
    // Obtiene referencia de los objetos que necesita
    let input = e.target;    
    let hinput = input._hinput;
    let datalist = input._datalist;
    
    copiarId(input.value, datalist, hinput);
}

/**
 * Copia el ID del datalist al hidden en caso de que el texto escrito
 * coincida con alguna de las opciones
 */
function copiarId(value, datalist, hinput) {

    // Recorre las opciones y comprueba a ver cuál es la que ha seleccionado
    let options = document.querySelectorAll('#'+datalist.id+' > option');
    for(let o of options) {

        // Si ha encontrado el campo, copia el valor del indice
        if(o.innerText == value) {
            hinput.value = o.attributes['data-value'].value;                
            break;
        }
    }
}


/** 
 * Vacía el DataList
 */
function vaciarDataList(datalist) {
    $(datalist).empty();
}

/**
 * Utilizando la URL pasada como argumento, descarga los datos y los
 * inserta en el datalist
 * 
 * @param {*} value 
 * @param {*} datalist 
 * @param {*} url 
 */
function actualizarDataList(value, datalist, url) {

    // Vacía la lista. Descarga ahora la lista desde el servidor
    vaciarDataList(datalist);

    // Crea un filtro que obtenga todos los usuarios que empiezan
    // Por el dato introducido por el usuario
    let filtro = value + '%';

    // Creo un objeto con los parámetros
    const parametros = {
        filtro: filtro
    };

    // Llama ahora 
    fetch(
        url, 
        {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(parametros), // data can be `string` or {object}!
          headers:{
              'Content-Type': 'application/json'
        }
      })
      .then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => mostrarResultado(response, datalist));  
}

/** 
 * Muestra el resultado en el datalist
 */
 function mostrarResultado(resultado, datalist) {    

    if(resultado.ok != 0) {        
        // Si el resultado está ok, 
        datalist.innerHTML = json2html.render(resultado.datos, PLANTILLA_DATALIST);
    } else {
        mostrarAlert(resultado.mensaje);
    }
   
}

