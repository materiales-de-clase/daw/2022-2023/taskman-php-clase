// -----------------------------------------------------------------------
// Implementa la Descarga de un SELECT utilizando AJAX
// Requiere para funcionar lo siguiente:
//  - El select
//  - La URL de descarga a la que se va a enviar contenido del input para hacer
//    la búsqueda
// -----------------------------------------------------------------------
export { axInicializarSelect , axInicializarSelectEsclavo, axAsignarSelectEsclavo };

const PLANTILLA_SELECT = {'<>': 'option','value':'${id}','html': '${texto}' };

/**
 * Inicializa el SELECT y lo deja cargado con los datos
 * 
 * @param {*} select 
 * @param {*} url
 */
function axInicializarSelect(select, url) {
    cargarSelect(select, url);
}

/**
 * Inicializa el SELECT esclavo. Este select solo se carga cuando
 * el select principal cambia. Dicho cambio usará el principal como clave
 * 
 * @param {*} maestro el select que aporta la clave
 * @param {*} esclavo el select que debemos cargar
 * @param {*} url
 * @param {*} seleccionado es el valor seleccionado en el combo. Opcional
 */
 function axInicializarSelectEsclavo(maestro, esclavo, url, seleccionado) {
    
    esclavo.url = url;
    esclavo.maestro = maestro;
    esclavo.disabled = true;

    // Asigna el gestor de evento para cuando cambia el maestro
    maestro.addEventListener('change', (e) => {
                
        esclavo.disabled = false;

        cargarSelect(esclavo, url, maestro.value, seleccionado);
    });
}

/**
 * Asigna un valor a un select esclavo, provoca la carga desde el servidor
 * y lo activa
 * 
 * @param {*} esclavo
 * @param {*} seleccionado 
 */
function axAsignarSelectEsclavo(esclavo, seleccionado) {

    // Debe estar activo
    esclavo.disabled = false;
    
    // Carga los datos
    cargarSelect(esclavo, esclavo.url, esclavo.maestro.value, seleccionado);
}


/** 
 * Vacía el Select
 */
function vaciarSelect(select) {
    $(select).empty();
}

/**
 * Utilizando la URL pasada como argumento, descarga los datos y los
 * inserta en el select
 * 
 * @param {*} select
 * @param {*} url 
 * @param {*} arg opcional. Por defecto %
 * @param {*} arg opcional. Indica la opción seleccionada
 */
function cargarSelect(select, url, arg = '%', seleccionado) {

    // Vacía la lista. 
    vaciarSelect(select);

    // Creo un objeto con los parámetros
    const parametros = {
        filtro: arg
    };

    // Llama ahora 
    fetch(
        url, 
        {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(parametros), // data can be `string` or {object}!
          headers:{
              'Content-Type': 'application/json'
        }
      })
      .then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => mostrarResultado(response, select, seleccionado));  
}

/** 
 * Muestra el resultado en el datalist
 */
 function mostrarResultado(resultado, select, seleccionado) {    

    if(resultado.ok != 0) {        
        // Si el resultado está ok, 
        select.innerHTML = '<option value="-1">Escoja opción</option>';
        select.innerHTML = select.innerHTML += json2html.render(resultado.datos, PLANTILLA_SELECT);

        // Ahora selecciona el elemento que se indica si se había pasado como argumento
        if(seleccionado) {
            select.value = seleccionado;
        }

    } else {
        mostrarAlert(resultado.mensaje);
    }
   
}

