
const URL_ELIMINAR = 'http://localhost/ajax.php/ajax.php?s=_deleteTarea&__debug';


export function axEliminarTarea(id, onOk, onError) {
    
    // Creo los parámetros
    const parametros = {
        id: id
    }

    // Llamo al servicio eliminar tarea
    fetch(
        URL_ELIMINAR, 
        {
            method: 'POST',
            body: JSON.stringify(parametros),
            headers: {
                'Content-Type': 'application/json'
            }
        }        
    )
    .then(respuesta => respuesta.json())
    .then(respuestaObj => {
        if(respuestaObj.ok == 1) {
            onOk();
        } else {
            onError(respuestaObj.mensaje);
        }
    });
}

/**
 * Carga un registro desde la base de datos
 * 
 * @param {*} url 
 * @param {*} id 
 * @param {*} onOk 
 * @param {*} onError 
 */
export function axGetRegistro(url, id, onOk, onError) {

    const parametros = {
        id: id
    }

    fetch(
        url, 
        {
            method: 'POST',
            body: JSON.stringify(parametros),
            headers: {
                'Content-Type': 'application/json'
            }
        }
    )
    .then(respuesta => respuesta.json())
    .then(respuesta => {

        // Procesa la respuesta
        if(respuesta.ok == 1) {
            onOk(respuesta.datos);
        } else {
            onError(respuesta.mensaje);
        }
    });
}
