

export function mostrarAlert(mensaje) {

    const _mostrarAlert = () => {
        $('#modalAlert #modalAlertText').text(mensaje);
        $('#modalAlert').modal('show');            
    }

    if(!$('#modalAlert').length) {

        // Cargo el HTML en el documento
        $('body').append( $('<div>').load("iu/dialogo/alert.php", _mostrarAlert ));                        
    
    } else {
        // Muestra el alert
        _mostrarAlert();
    }
}

export function mostrarModalEliminar(mensaje, accion /* Es una función */) {
    
    const _mostrarModalEliminar = () => {
        $('#modalEliminar #modalEliminarTexto').text(mensaje);
        $('#modalEliminar').modal('show');    

        $('#modalEliminarBotonAceptar').on('click', (event) => {
        
            // Ejecutar la acción que me han pasado como argumento
            accion();
            
            // Oculto el cuaro de diáogo
            $('#modalEliminar').modal('hide');    
    
            // Quito el gesto de evento click
            $('#modalEliminar #modalEliminarBotonAceptar').off('click');
        });        
    }
    
    if(!$('#modalEliminar').length) {

        // Cargo el HTML en el documento
        $('body').append( $('<div>').load("iu/dialogo/eliminar.php", _mostrarModalEliminar ));                        
    } else {
        _mostrarModalEliminar();
    }

    
}
