// Este módulo implementa validaciones de formulario. Permite hacer validaciones
// utilizando el atributo validación de un formulario
// Pemite implementar validaciones de formulario de forma sencilla
// Para mapear las validaciones, se debe utilizar el atributo validacion
// asignándolo a una función de validación válida. 
// En este fichero se encontrarán todas las funciones de validación implementadas

// Basado en documentación de validaciones de Bootstrap para hacer la integración
// https://getbootstrap.com/docs/5.1/forms/validation/

//-------------------------------------------------------------------------------------
// API del módulo
//-------------------------------------------------------------------------------------

/**
 * Inicializa las validaciones por campo del formulario
 */
export function frmInicializarFormulario(formularioRef) {
   
    // Para cada elemento con un atributo validacion, llama a fInicializarElemento
    formularioRef.querySelectorAll("#"+formularioRef.id+" [validacion]").forEach((c) => {
        inicializarCampo(c);
    });
}

export function frmValidarFormulario(formularioRef, onOk, onError) {        
    (async function () {

        // Formulario ya que el evento submit ocurre sobre el form
        // Esto es válido cuando gestionamos el submit. En este caso
        // el evento es en el botón
        // const formulario = evento.target;
        //const formulario = $(evento.target).parents('form')[0];

        // Limpia los mensajes de error que pudiéramos tener
        limpiarErrores(formularioRef);

        // En primer lugar, comprueba si se cumplen las restricciones de formulario. Por ejemplo, 
        // si todos los campos requeridos están incluidos
        formularioRef.checkValidity(); 

        // Errores que han ocurrido
        let numeroErrores = 0;

        // Obtener todos los campos en el formulario que tienen validacion
        // Y validarlos. Utiliza para ello un selector CSS específico
        const elementos = formularioRef.querySelectorAll("[validacion]");
        for(const e of elementos) {
                
            // Valida un campo
            numeroErrores += await validarCampoAsync(e);
        }

        // Si no hay errores, envía el formulario
        if(numeroErrores == 0) {                        
            // Si no usara AJAX, haría esto, pero al usar AJAX se hace necesario
            // hacer el envío de datos de otro modo
            // formulario.alta.value = '1';
            // formulario.submit();

            // Envía los datos del formulario
            //enviarFormulario(formulario);

            onOk(formularioRef);

        } else {
            
            // Si hay errores, marca que se ha validado. De este modo se marcan 
            // los campos con errores. Esto lo hace            
            // añadiendo la clase indicando que ya se ha validado el formulario
            //formularioRef.classList.add('was-validated');

            // Llama ahora al gestor de errores
            onError(formularioRef);
        }
    })();
}





//-------------------------------------------------------------------------------------
// Funciones para trabajar con errores
//-------------------------------------------------------------------------------------

/**
 * Muestra un mensaje de error
 * 
 * @param {*} error 
 */
 function mostrarError(elemento, error) {
    $(elemento).addClass('is-invalid');
    $('#'+elemento.id+' + .invalid-feedback').text(error);
}

/**
 * Limpia los errores del formulario
 */
function limpiarErrores(formulario) {
    
    // Marca que el formulario no se ha validado
    formulario.classList.remove('was-validated');

    // Marca todos los elementos como válidos
    formulario.querySelectorAll("input[validacion]").forEach((elemento) => {
        $(elemento).removeClass('is-invalid');
    });        
}



//-------------------------------------------------------------------------------------
// Funciones para validación de campos
//-------------------------------------------------------------------------------------

/**
 * Inicializa un elemento pasado como argumento. La inicialización asigna
 * el evento blur al gestor de evento que va a validar el campo.
 * 
 * @param {*} elemento 
 */
 function inicializarCampo(elemento) {

    // Cuando se abandona el campo (blur) se llama a la función validar campo
    elemento.addEventListener("blur", validarCampo);
}


/**
 * Valida el campo sobre el que se ha producido el evento
 * 
 * @param {*} evento 
 */
 function validarCampo(evento) {
    
    // Obtiene el elemento sobre el que se ha lanzado el evento y llama
    // a realizar la validación
    validarCampoAsync(evento.target);
}

/**
 * Valida un campo recibido como argumento de forma asíncrona y 
 * muestra el error que corresponda en la página web
 * 
 * @param {*} e elemento html que contiene el dato a validar. Puede ser un input
 */
async function validarCampoAsync(e) {
    
    let numeroErrores = 0;

    try {

        // Elimina el error 
        $(e).removeClass('is-invalid');

        // Validación que se aplicaría al campo
        const validacion = e.attributes['validacion'].value;

        if(validacion) {
            // Valor en el campo
            const valor = e.value;

            // Obtengo el resultado de la validación
            //await window['validacion_'+validacion](valor);
            await eval("validacion_"+validacion+"('"+valor+"', e)");
        }
    } catch(exception) {
         
        // Si no pasa la validación
         mostrarError(e, exception);
         numeroErrores++;
    }
   
    // Retorna el número de errores encontrados
    return numeroErrores;
}


//--------------------------------------------------------------------------
// Validaciones
//--------------------------------------------------------------------------

/**
 * Falla si un campo está vacío
 * 
 * @param {*} v 
 */
 async function validacion_novacio(v) {
    if(v.trim().length == 0) {
        throw "El campo está vacío";
    }       
}

/**
 * Valida un nombre. 
 * 
 * @param {*} v true si el nombre es válido o el mensaje de error.
 */
function validacion_nombre(v) {
    validacion_novacio(v);

    //throw "El nombre no es válido";
}

/**
 * Valida un apellido. 
 * 
 * @param {*} v true si el apellidos es válido o el mensaje de error.
 */
function validacion_apellido(v) {
    validacion_novacio(v);

    //throw "El apellidos no es válido";
}

/**
 * Valida que el nif es correccto. Para ello comprueba la letra de control
 * @param {*} v 
 */
function validacion_nif(v) {
    validacion_novacio(v);

    if(!validarNIF) {
        throw "El nif introducido no es correcto";
    }    
}

/**
 * Valida que el nif es correccto. Para ello comprueba la letra de control
 * @param {*} v 
 */
function validacion_iban(v) {
    //throw "El IBAN introducido no es correcto";
}

/**
 * Código postal
 * 
 * @param {*} v 
 */
 function validacion_codigoPostal(v) {
    //throw "El código postal introducido no es correcto";
}

/**
 * móvil
 * 
 * @param {*} v 
 */
function validacion_telefonoMovil(v) {
    return true;
}

/**
 * Valida que el NIF pasado como argumento no existe en la base de datos de clientes.
 * 
 * @param {*} v 
 * @returns 
 */
async function validacion_nifClienteNoExiste(v) {
    
    validacion_nif(v);

    // Si el NIF es correcto, valido que el cliente no exista.
    const parametros = {
        nif: v
    };
   
    const respuesta = await fetch('ajax.php?script=validacion_clienteExiste', {
        
        method: 'POST', 
        body: JSON.stringify(parametros), 
        headers:{
            'Content-Type': 'application/json'
        }
    });
    const objeto = await respuesta.json();

    if(objeto.resultado) {
        throw 'El cliente ya existe';
    }
}

/**
 * Validación avanzada que permite validar un campo a partir de otro campo
 * 
 * @param {*} v 
 */
async function validacion_idvalido(v, e) {

    // Obtiene el ID del campo que hay que validar
    if(e.attributes['campo_validacion'] != undefined) {
        let id_campo = e.attributes['campo_validacion'].value;
        let campo = document.getElementById(id_campo);
        if(!campo || campo.value < 0) {
            throw "El identificador no es válido";
        }
    } else {
        if(v < 0) {
            throw "El identificador no es válido";
        }
    }
}


