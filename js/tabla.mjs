
export { AxTabla };

class AxTabla {

    #plantilla;  // Plantilla que vamos a utilizar para renderizar la tabla
    #url;        // URL con el origen de la información
    #idDestino   // Identificador del elemento donde se va a insertar la tabla

    #filtro;     // Filtro. Filtro que tiene la tabla aplicado en estos momentos

    constructor(plantilla, url, idDestino) {
        this.#plantilla = plantilla;
        this.#url = url;
        this.#idDestino = idDestino;
    }

    renderizar(filtro = '%') {
        this.#filtro = filtro;
    
        // Ajusta el filtro añadiendo %
        if(!filtro.endsWith('%')) {
            filtro = filtro + '%';
        }
    
        // Crear un objeto con los parámetros
        const parametros = {
            filtro: filtro
        }
        
        // Llamar a la url del nuestro servicio
        fetch(
            this.#url,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(parametros)
            }
        )
        .then(respuesta => respuesta.json())
        .then(respuestaObj => {
    
            // respuestaObj es el objeto con el resultado
            this.#mostrarResultado(respuestaObj);    
        });
    }

    #mostrarResultado(respuestaObj) {
        if(respuestaObj.ok == '1') {
    
            // Mostrar el resultado en la tabla
            const html = json2html.render(respuestaObj.datos, this.#plantilla);
            $(`#${this.#idDestino}`).html(html);
    
        } else {
    
            // Mostramos un mensaje de error
            mostrarAlert(respuestaObj.mensaje);
        }
    }    
}