<?php
class LnEstadosTipoTarea {
		
		private static $instancia;
		private $db;

		function __construct() {
			$this->db = Conexion::singleton_conexion();
		}

		public static function singletonLnEstadosTipoTarea(){
			if(!isset(self::$instancia)){
				$miclase= __CLASS__;
				self::$instancia = new $miclase;
			}
			return self::$instancia;
		}

		public function getSelectEstadosTareaPorTipo($filtro){

			// Carga el singleton 
			$estadostipotarea = EstadosTipoTarea::singletonEstadosTipoTarea();

			// Carga la tabla 
			$r = $estadostipotarea->getSelectEstadosTipoTarea($filtro);

			// Retorna la tabla de tareas 
			return $r;
		}
	}
?>