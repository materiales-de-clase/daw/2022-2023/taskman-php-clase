<?php
class LnTareas {
		
		private static $instancia;
		private $db;

		function __construct() {
			$this->db = Conexion::singleton_conexion();
		}

		public static function singletonTareas(){
			if(!isset(self::$instancia)){
				$miclase= __CLASS__;
				self::$instancia = new $miclase;
			}
			return self::$instancia;
		}

		/**
		 * En caso de error este método podría lanzar una excepción
		 */
		public function addTarea(Tarea $t){
			// Carga el singleton de tareas
			$tareas = Tareas::singletonTareas();

			// Añade la tarea
			$tareas->addTarea($t);
			
			return $t;
		}

		/**
		 * Actualiza la tarea
		 */
		public function setTarea(Tarea $t){
			
			// Carga el singleton de tareas
			$tareas = Tareas::singletonTareas();

			// Añade la tarea
			$tareas->setTarea($t);
			
			return $t;
		}

		public function getTareaPorId($id){
			
			// Carga el singleton de tareas
			$tareas = Tareas::singletonTareas();

			// Carga la tabla de tareas
			$r = $tareas->getTareaPorId($id);

			// Retorna la tarea
			return $r;
		}

		public function getListadoTareasFiltradasPorTitulo($filtro){

			// Carga el singleton de tareas
			$tareas = Tareas::singletonTareas();

			// Carga la tabla de tareas
			$r = $tareas->getListadoTareasFiltradasPorTitulo($filtro);

			// Retorna la tabla de tareas 
			return $r;
		}

		public function getResumenTareasPorEstado() {
			// Carga el singleton de tareas
			$tareas = Tareas::singletonTareas();

			// Carga la tabla de tareas
			$r = $tareas->getResumenTareasPorEstado();

			// Retorna la tabla de tareas 
			return $r;
		}

		public function deleteTarea($id){
			
			// Carga el singleton de tareas
			$tareas = Tareas::singletonTareas();

			// Carga la tabla de tareas
			$r = $tareas->deleteTarea($id);

			// Retorna la tarea
			return $r;
		}

	}
?>