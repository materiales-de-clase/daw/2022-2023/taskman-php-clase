<?php
class LnTiposTarea {
		
		private static $instancia;
		private $db;

		function __construct() {
			$this->db = Conexion::singleton_conexion();
		}

		public static function singletonLnTiposTarea(){
			if(!isset(self::$instancia)){
				$miclase= __CLASS__;
				self::$instancia = new $miclase;
			}
			return self::$instancia;
		}

		public function getSelectTiposTarea($filtro){

			// Carga el singleton 
			$tipostarea = TiposTarea::singletonTiposTarea();

			// Carga la tabla 
			$r = $tipostarea->getSelectTiposTarea($filtro);

			// Retorna la tabla de tareas 
			return $r;
		}
	}
?>