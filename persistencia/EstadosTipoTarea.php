<?php
class EstadosTipoTarea {
		
		private static $instancia;
		private $db;

		function __construct() {
			$this->db = Conexion::singleton_conexion();
		}

		public static function singletonEstadosTipoTarea() {
			if(!isset(self::$instancia)){
				$miclase= __CLASS__;
				self::$instancia = new $miclase;
			}
			return self::$instancia;
		}


		/**
		 * Dado el filtro a aplicar sobre el nombre del tipo, devuelve
		 * los tipos
		 */
		public function getSelectEstadosTipoTarea($filtro) {
        				
			// Prepara la consulta a la base de datos
			$query=$this->db->preparar($this::SQL_SELECT_ESTADOS_TIPO_TAREA);

			// Asigna los parámetros a la consulta. Reemplaza las ?
			// por los valores pasados como argumento
			$query->bindParam(1,$filtro);
			
			// Lanza la consulta contra la BD
			$query->execute();

			// Carga el resultado de la consulta
			$tEstados=$query->fetchall();

			// Retorna la tabla con el resultado.
			// El resultado puede ser un tabla vacía perfectamente 
			return $tEstados;
		}

		//--------------------------------------------------------------------
		// CONSULTAS SQL
		//--------------------------------------------------------------------
		const SQL_SELECT_ESTADOS_TIPO_TAREA = <<<SQL
			SELECT
				id_estado as id, 
				nombre as texto
			FROM estados_tipo_tarea tt
			where tt.id_tipo_tarea = ?;
		SQL;		

	}
?>