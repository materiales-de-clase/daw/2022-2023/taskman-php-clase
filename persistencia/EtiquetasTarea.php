<?php
class EtiquetasTarea{
		
		private static $instancia;
		private $db;

		function __construct() {
			$this->db = Conexion::singleton_conexion();
		}

		public static function singletonEtiquetasTarea(){
			if(!isset(self::$instancia)){
				$miclase= __CLASS__;
				self::$instancia = new $miclase;
			}
			return self::$instancia;
		}

		public function addEtiquetaTarea(EtiquetaTarea $e){

			$consulta="INSERT INTO etiquetas_tarea (id_tarea, id_etiqueta) 
						VALUES(?,?)";
			
			$query=$this->db->preparar($consulta);				
				$query->bindParam(1, $e->getIdTarea());	
				$query->bindParam(1, $e->getIdEtiqueta());

			// Si no se ha podido insertar lanza una excepción
			if(!$query->execute()) {
				throw new Exception("No se ha podido insertar la etiqueta de tarea");
			}
			
			return $e;
		}
	}
?>