<?php
class Tareas{
		
		private static $instancia;
		private $db;

		function __construct() {
			$this->db = Conexion::singleton_conexion();
		}

		public static function singletonTareas(){
			if(!isset(self::$instancia)){
				$miclase= __CLASS__;
				self::$instancia = new $miclase;
			}
			return self::$instancia;
		}

		public function addTarea(Tarea $t){

			$consulta="INSERT INTO tareas (id_tarea, titulo, id_informador, id_asignado, tipo, estado, descripcion, fecha_alta, fecha_vencimiento, hora_vencimiento) 
						VALUES(null,?,?,?,?,?,?,?,?,?)";			

			$titulo = $t->getTitulo();
			$idInformador = $t->getIdInformador();
			$idAsignado = $t->getIdAsignado();
			$tipo = $t->getTipo();
			$estado = $t->getEstado();
			$descripcion = $t->getDescripcion();
			$fechaAlta = $t->getFechaAlta();
			$fechaVencimiento = $t->getFechaVencimiento();
			$horaVencimiento = $t->getHoraVencimiento();

			$query=$this->db->preparar($consulta);				
				$query->bindParam(1, $titulo);
				$query->bindParam(2, $idInformador);
				$query->bindParam(3, $idAsignado);
				$query->bindParam(4, $tipo);
				$query->bindParam(5, $estado);
				$query->bindParam(6, $descripcion);
				$query->bindParam(7, $fechaAlta);
				$query->bindParam(8, $fechaVencimiento);
				$query->bindParam(9, $horaVencimiento);

			// Si no se ha podido insertar lanza una excepción
			if(!$query->execute()) {
				throw new Exception("No se ha podido insertar la tarea");
			}
			
			// Toma el ID que se ha generado en la BD y lo pone el objeto
			$t->setIdTarea($this->db->getUltimoId());

			return $t;
		}

		public function setTarea(Tarea $t){

			$idTarea = $t->getIdTarea();
			$titulo = $t->getTitulo();
			$idInformador = $t->getIdInformador();
			$idAsignado = $t->getIdAsignado();
			$tipo = $t->getTipo();
			$estado = $t->getEstado();
			$descripcion = $t->getDescripcion();
			$fechaAlta = $t->getFechaAlta();
			$fechaVencimiento = $t->getFechaVencimiento();
			$horaVencimiento = $t->getHoraVencimiento();

			$query=$this->db->preparar($this::SQL_UPDATE_TAREA);				
				$query->bindParam(1, $titulo);
				$query->bindParam(2, $idInformador);
				$query->bindParam(3, $idAsignado);
				$query->bindParam(4, $tipo);
				$query->bindParam(5, $estado);
				$query->bindParam(6, $descripcion);
				$query->bindParam(7, $fechaAlta);
				$query->bindParam(8, $fechaVencimiento);
				$query->bindParam(9, $horaVencimiento);

				$query->bindParam(10, $idTarea);

			// Si no se ha podido insertar lanza una excepción
			if(!$query->execute()) {
				throw new Exception("No se ha podido insertar la tarea");
			}
			
			return $t;
		}

		public function getTareaPorId($id){
			
			// Prepara la consulta a la base de datos
			$query=$this->db->preparar($this::SQL_TAREA_POR_ID);

			// Asigna los parámetros a la consulta. Reemplaza las ?
			// por los valores pasados como argumento
			$query->bindParam(1,$id);
			
			// Lanza la consulta contra la BD
			$query->execute();

			// Carga el resultado de la consulta
			$tTareas=$query->fetchall();

			// Retorna el primer registro de la tabla
			return $tTareas[0];
		}


		public function deleteTarea($id){
			
			// Prepara la consulta a la base de datos
			$query=$this->db->preparar($this::SQL_DELETE_TAREA);

			// Asigna los parámetros a la consulta. Reemplaza las ?
			// por los valores pasados como argumento
			$query->bindParam(1, $id);
			
			// Lanza la consulta contra la BD
			$query->execute();

			// Carga el resultado de la consulta
			$eliminados = $query->rowCount();

			// Retorna la tabla con el resultado.
			// El resultado puede ser un tabla vacía perfectamente 
			return $eliminados == 1;
		}
		

		/**
		 * Dado el filtro a aplicar sobre el título de la tarea, selecciona todas las tareas
		 * que dan coincidencia.
		 * 
		 * En el listado, se resuelven los datos que proceden de otras tablas. Por ejemplo,
		 * el informador o el usuario asignado, así como tipo, estado, etc.
		 */
		public function getListadoTareasFiltradasPorTitulo($filtro){
        				
			// Prepara la consulta a la base de datos
			$query=$this->db->preparar($this::SQL_LISTADO_TAREAS_FILTRADAS_POR_TITULO);

			// Asigna los parámetros a la consulta. Reemplaza las ?
			// por los valores pasados como argumento
			$query->bindParam(1,$filtro);
			
			// Lanza la consulta contra la BD
			$query->execute();

			// Carga el resultado de la consulta
			$tTareas=$query->fetchall();

			// Retorna la tabla con el resultado.
			// El resultado puede ser un tabla vacía perfectamente 
			return $tTareas;
		}

		public function getResumenTareasPorEstado(){
        				
			// Prepara la consulta a la base de datos
			$query=$this->db->preparar($this::SQL_RESUMEN_TAREAS_POR_ESTADO);
			
			// Lanza la consulta contra la BD
			$query->execute();

			// Carga el resultado de la consulta
			$tTareas=$query->fetchall();

			// Retorna la tabla con el resultado.
			// El resultado puede ser un tabla vacía perfectamente 
			return $tTareas;
		}


		//--------------------------------------------------------------------
		// CONSULTAS SQL
		//--------------------------------------------------------------------
		const SQL_TAREA_POR_ID = <<<SQL
			SELECT
				id_tarea, 
				titulo, 
				t.id_informador,
				ui.usuario as informador, 
				t.id_asignado,
				ua.usuario as asignado, 
				t.tipo as id_tipo_tarea,
				tt.nombre as tipo, 
				t.estado as id_estado,
				ett.nombre as estado, 
				descripcion, 
				fecha_alta, 
				fecha_vencimiento, 
				hora_vencimiento
			FROM tareas t
				inner join usuarios ui on t.id_informador = ui.id_usuario 
				inner join usuarios ua on t.id_asignado = ua.id_usuario 
				inner join tipos_tarea tt on t.tipo = tt.id_tipo_tarea 
				inner join estados_tipo_tarea ett on t.estado = ett.id_estado 	
			where t.id_tarea = ?;
		SQL;

		const SQL_LISTADO_TAREAS_FILTRADAS_POR_TITULO = <<<SQL
			SELECT
				id_tarea, 
				titulo, 
				t.id_informador,
				ui.usuario as informador, 
				t.id_asignado,
				ua.usuario as asignado, 
				t.tipo as id_tipo_tarea,
				tt.nombre as tipo, 
				t.estado as id_estado,
				ett.nombre as estado, 
				descripcion, 
				fecha_alta, 
				fecha_vencimiento, 
				hora_vencimiento
			FROM tareas t
				inner join usuarios ui on t.id_informador = ui.id_usuario 
				inner join usuarios ua on t.id_asignado = ua.id_usuario 
				inner join tipos_tarea tt on t.tipo = tt.id_tipo_tarea 
				inner join estados_tipo_tarea ett on t.estado = ett.id_estado 	
			where t.titulo like ?;
		SQL;

		const SQL_DELETE_TAREA = <<<SQL
			delete from tareas where id_tarea = ?;
		SQL;	

		const SQL_RESUMEN_TAREAS_POR_ESTADO = <<<SQL
			SELECT
				count(id_tarea) as contador, 
				ett.nombre as estado
			FROM tareas t
				inner join estados_tipo_tarea ett on t.estado = ett.id_estado 	
			group by ett.nombre
		SQL;

		const SQL_UPDATE_TAREA = <<< SQL
			UPDATE tareas set 
				titulo = ?, 
				id_informador = ?, 
				id_asignado = ?, 
				tipo = ?, 
				estado = ?, 
				descripcion = ?, 
				fecha_alta = ?, 
				fecha_vencimiento = ?, 
				hora_vencimiento = ? 
			WHERE id_tarea = ?
		SQL;
	}
?>