<?php
class TiposTarea {
		
		private static $instancia;
		private $db;

		function __construct() {
			$this->db = Conexion::singleton_conexion();
		}

		public static function singletonTiposTarea() {
			if(!isset(self::$instancia)){
				$miclase= __CLASS__;
				self::$instancia = new $miclase;
			}
			return self::$instancia;
		}


		/**
		 * Dado el filtro a aplicar sobre el nombre del tipo, devuelve
		 * los tipos
		 */
		public function getSelectTiposTarea($filtro) {
        				
			// Prepara la consulta a la base de datos
			$query=$this->db->preparar($this::SQL_SELECT_TIPOS_TAREA);

			// Asigna los parámetros a la consulta. Reemplaza las ?
			// por los valores pasados como argumento
			$query->bindParam(1,$filtro);
			
			// Lanza la consulta contra la BD
			$query->execute();

			// Carga el resultado de la consulta
			$tTipos=$query->fetchall();

			// Retorna la tabla con el resultado.
			// El resultado puede ser un tabla vacía perfectamente 
			return $tTipos;
		}

		//--------------------------------------------------------------------
		// CONSULTAS SQL
		//--------------------------------------------------------------------
		const SQL_SELECT_TIPOS_TAREA = <<<SQL
			SELECT
				id_tipo_tarea as id, 
				nombre as texto
			FROM tipos_tarea tt
			where tt.nombre like ?;
		SQL;		

	}
?>