<?php 

class EtiquetaTarea{

	private $idEtiqueta;
    private $idTarea;
	
	public function __construct($idEtiqueta, $idTarea)
	{
		$this->idEtiqueta = $idEtiqueta;
		$this->idTarea = $idTarea;
	}
  


	/**
	 * Get the value of idEtiqueta
	 */ 
	public function getIdEtiqueta()
	{
		return $this->idEtiqueta;
	}

	/**
	 * Set the value of idEtiqueta
	 *
	 * @return  self
	 */ 
	public function setIdEtiqueta($idEtiqueta)
	{
		$this->idEtiqueta = $idEtiqueta;

		return $this;
	}


    /**
     * Get the value of idTarea
     */ 
    public function getIdTarea()
    {
        return $this->idTarea;
    }

    /**
     * Set the value of idTarea
     *
     * @return  self
     */ 
    public function setIdTarea($idTarea)
    {
        $this->idTarea = $idTarea;

        return $this;
    }
 }
 ?>
