<?php 

class TipoTarea{

	private $idTipoTarea;
	private $nombre;
	
	public function __construct($idTipoTarea, $nombre)
	{
		$this->idTipoTarea = $idTipoTarea;
		$this->nombre = $nombre;
	}
  

	/**
	 * Get the value of id_tipo_tarea
	 */ 
	public function getIdTipoTarea()
	{
		return $this->idTipoTarea;
	}

	/**
	 * Get the value of nombre
	 */ 
	public function getNombre()
	{
		return $this->nombre;
	}

	/**
	 * Set the value of nombre
	 *
	 * @return  self
	 */ 
	public function setNombre($nombre)
	{
		$this->nombre = $nombre;

		return $this;
	}

	/**
	 * Set the value of idTipoTarea
	 *
	 * @return  self
	 */ 
	public function setIdTipoTarea($idTipoTarea)
	{
		$this->idTipoTarea = $idTipoTarea;

		return $this;
	}
 }
 ?>
